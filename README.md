gitlesk
=======

Gitlesk reads data from a gitlab instance and collects them into an
[Elasticsearch](https://github.com/elastic/elasticsearch) cluster. One can then draw
dashboards with [Kibana](https://github.com/elastic/kibana) as those below.

Gitlesk also keeps timed data from a Gitlab instance: you can then build dashboards based on these
timeseries data.

At the moment, gilesk is a Python script. It will move to [LibBeat](https://github.com/elastic/beats) asap.

Usage
-----

```
$ ./scripts/gitlesk.py --help
usage: ./scripts/gitlesk.py [-h] [--debug] [--es ES] [--teams TEAMS]

Extract gitlab data to ElasticSearch

optional arguments:
  -h, --help     show this help message and exit
  --debug        print debug messages
  --es ES        ElasticSearch endpoint as host:port.
  --teams TEAMS  File listing team members.
```

With the `teams` file, you can add one field in the user description: `teams`. This may be useful to group statistics
for users of the same team.

### teams file format

The teams file is a JSON file following this format:

```json
{
  "teams": [
    {
      "team": "first team",
      "members": [
        "UserName1",
        "UserName2",
        ...
        "UserNameN",
      ]
    },
    {
      "team": "second team",
      "members": [
        "UserName2",
        "UserName4",
        ...
        "UserNameM",
      ]
    }
  ]
}
```


samples
-------

These are the very first dashboards made with a POC of gitlesk.

![](images/projects.png)

![](images/issues.png)
