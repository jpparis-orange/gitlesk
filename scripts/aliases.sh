#!/bin/bash


ES=${1:-localhost:9200}

curl -XPOST "http://$ES/_aliases" -d '
{
    "actions" : [
        { "add" : { "index" : "gitlab-a-day", "alias" : "gitlab-no-ts" } }
    ]
}'
