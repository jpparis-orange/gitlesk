#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
from collections import defaultdict
from datetime import datetime, tzinfo, date, timedelta
import argparse
import re
import sys

import gitlab
# https://github.com/gpocentek/python-gitlab
# GET /projects/search/:query
# GET /projects/:id/repository/branches

from elasticsearch import Elasticsearch

######################################################################
# command line arguments
def parseArgs(p_args = sys.argv):
  l_parser = argparse.ArgumentParser(
    prog=p_args[0],
    description="Extract gitlab data to ElasticSearch",
  )
  l_parser.add_argument(
    "--debug", action='store_true', default=False,
    help="print debug messages",
  )
  l_parser.add_argument(
    "--es", type=str,
      default="localhost:9200",
      help="ElasticSearch endpoint as host:port.",
  )
  l_parser.add_argument(
    "--teams", type=argparse.FileType('r', encoding='UTF-8'),
      help="File listing team members.",
  )
  l_result = l_parser.parse_args(p_args[1:])
  return l_result

######################################################################
# team
def readTeamFile(p_jsonFile):
  l_result = defaultdict(lambda: list())
  l_json = json.load(p_jsonFile)
  for c_team in l_json["teams"]:
    l_teamName = c_team["team"]
    for c_member in c_team["members"]:
      l_result[c_member].append(l_teamName)
  return l_result

######################################################################
# anonymisation data used to generate screen shots for the web
g_anon = False
g_anonDict = defaultdict(lambda: dict())
def anon(p_type, p_value):
  if p_value not in g_anonDict[p_type]:
    g_anonDict[p_type][p_value] = p_type + "_" + str(len(g_anonDict[p_type]))
  return g_anonDict[p_type][p_value]

######################################################################
# bulk
def bulkInject(p_es, p_docs):
  l_result = p_es.bulk(p_docs)
  if l_result['errors']:
    print("*** Error: bulk", l_result, file=sys.stderr)
    sys.exit(1)

######################################################################
def mergeGroups(p_g1, p_g2):
  l_result = {}
  for c_key in p_g1:
    l_result[c_key] = p_g1[c_key]
  for c_key in p_g2:
    if c_key not in l_result:
      l_result[c_key] = p_g2[c_key]
  return l_result

def groups(p_gl, p_es, p_groups = {}):
  l_groups = []
  l_morePages = True
  l_page = 1
  while l_morePages:
    l_groupsPage = p_gl.groups.list(page=l_page, per_page=10)
    l_groups.extend(l_groupsPage)
    l_page += 1
    l_morePages = (0 != len(l_groupsPage))

    l_bulkData = []
    l_bulkSize = 500
    l_written = 0
  for c_group in l_groups:
    l_jsonGroup = json.loads(c_group.json())
    if l_jsonGroup["id"] in p_groups:
      l_jsonGroup = mergeGroups(l_jsonGroup, p_groups[l_jsonGroup["id"]])
      p_groups[l_jsonGroup["id"]] = l_jsonGroup
    if g_anon:
      l_jsonGroup["name"] = anon("group", l_jsonGroup["name"])

    l_bulkData.append({ "index":
                        {
                          "_index": "gitlab-a-day",
                          "_type" : "group",
                          "_id" : l_jsonGroup["id"]
                        }
                      })
    l_bulkData.append( {
      "@timestamp": l_jsonGroup.get("created_at", g_now), # why?
      "group": l_jsonGroup,
    } )
    if len(l_bulkData) > l_bulkSize:
      bulkInject(p_es, l_bulkData)
      l_written += len(l_bulkData) / 2
      l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(p_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "groups injected")

######################################################################
def projects(p_gl, p_es, p_users = {}):
  #l_projects = l_gl.projects.list(all=True)
  l_morePages = True
  l_page = 1

  l_bulkData = []
  l_bulkSize = 500
  l_written = 0

  while l_morePages:
    l_projectsPage = p_gl.projects.list(page=l_page, per_page=10)
    l_page += 1
    l_morePages = (0 != len(l_projectsPage))

    for c_project in l_projectsPage:
      g_projects.append(c_project)
      l_jsonProject = json.loads(c_project.json())
      if g_anon:
        l_jsonProject["name"]              = anon("project", l_jsonProject["name"])
        l_jsonProject["namespace"]["name"] = anon("group",   l_jsonProject["namespace"]["name"])

      l_ns = l_jsonProject.pop("namespace")
      g_groups[l_ns["id"]] = l_ns

      l_bulkData.append({ "index":
                          {
                            "_index": "gitlab-a-day",
                            "_type" : "project",
                            "_id" : l_jsonProject["id"]
                          }
                        })
      l_user = {}
      if l_jsonProject["creator_id"] in p_users:
        l_user = p_users[l_jsonProject["creator_id"]]
      l_bulkData.append( {
        "@timestamp": l_jsonProject["created_at"],
        "project": l_jsonProject,
        "group": l_ns,
        "user": l_user
      } )
      if len(l_bulkData) > l_bulkSize:
        bulkInject(p_es, l_bulkData)
        l_written += len(l_bulkData) / 2
        l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(p_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "projects injected")

######################################################################
def users(p_gl, p_es, p_userToTeam):
  #l_users = l_gl.users.list(all=True)
  l_morePages = True
  l_page = 1

  l_bulkData = []
  l_bulkSize = 500
  l_written = 0

  while l_morePages:
    l_usersPage = p_gl.users.list(page=l_page, per_page=10)
    l_page += 1
    l_morePages = (0 != len(l_usersPage))

    for c_user in l_usersPage:
      l_jsonUser = json.loads(c_user.json())
      g_users[l_jsonUser["id"]] = l_jsonUser

      if g_anon:
        l_jsonUser["name"] = anon("user", l_jsonUser["name"])
      if "created_at" not in l_jsonUser:
        l_jsonUser["created_at"] = g_now
      if l_jsonUser["name"] in p_userToTeam:
        l_jsonUser["teams"] = p_userToTeam[l_jsonUser["name"]]
      else:
        l_jsonUser["teams"] = []

      l_bulkData.append({ "index":
                          {
                            "_index": "gitlab-a-day",
                            "_type" : "user",
                            "_id" : l_jsonUser["id"]
                          }
                        })
      l_bulkData.append( {
        "@timestamp": l_jsonUser["created_at"],
        "user": l_jsonUser,
      } )
      if len(l_bulkData) > l_bulkSize:
        bulkInject(p_es, l_bulkData)
        l_written += len(l_bulkData) / 2
        l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(p_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "users injected")

######################################################################
def issues(p_gl, p_es, p_projects):
  l_bulkData = []
  l_bulkSize = 500
  l_written = 0

  for c_project in p_projects:
    l_morePages = True
    l_page = 1
    l_jsonProject = json.loads(c_project.json())
    if g_anon:
      l_jsonProject["name"]              = anon("project", l_jsonProject["name"])
      l_jsonProject["namespace"]["name"] = anon("group",   l_jsonProject["namespace"]["name"])
    l_ns = l_jsonProject.pop("namespace")

    while l_morePages:
      l_issuesPages = c_project.issues.list(page=l_page, per_page=10)
      l_page += 1
      l_morePages = (0 != len(l_issuesPages))

      for c_issue in l_issuesPages:
        l_jsonIssue = json.loads(c_issue.json())

        if g_anon:
          l_jsonIssue["author"]["name"]               = anon("user",    l_jsonIssue["author"]["name"])
          l_jsonIssue["assignee"]["name"]             = anon("user",    l_jsonIssue["assignee"]["name"])

        if l_jsonIssue["assignee"]:
          l_names = [l_jsonIssue["author"]["name"], l_jsonIssue["assignee"]["name"]]
          l_usernames = [l_jsonIssue["author"]["username"], l_jsonIssue["assignee"]["username"]]
        else:
          l_names = l_jsonIssue["author"]["name"]
          l_usernames = l_jsonIssue["author"]["username"]

        l_bulkData.append({ "index":
                            {
                              "_index": "gitlab-a-day",
                              "_type" : "issue",
                              "_id" : c_issue.id
                            }
                          })
        l_bulkData.append({
          "issue": l_jsonIssue,
          "project": l_jsonProject,
          "group": l_ns,
          "user": {
            "name": l_names,
            "username": l_usernames
          },
          "@timestamp": l_jsonIssue["created_at"]
        })

        if len(l_bulkData) > l_bulkSize:
          bulkInject(p_es, l_bulkData)
          l_written += len(l_bulkData) / 2
          l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(p_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "issues injected")

######################################################################
def milestones(p_gl, p_es, p_projects):
  l_bulkData = []
  l_bulkSize = 500
  l_written = 0

  for c_project in p_projects:
    l_morePages = True
    l_page = 1
    l_jsonProject = json.loads(c_project.json())
    if g_anon:
      l_jsonProject["name"]              = anon("project", l_jsonProject["name"])
      l_jsonProject["namespace"]["name"] = anon("group",   l_jsonProject["namespace"]["name"])
    l_ns = l_jsonProject.pop("namespace")

    while l_morePages:
      l_milestonesPages = c_project.milestones.list(page=l_page, per_page=10)
      l_page += 1
      l_morePages = (0 != len(l_milestonesPages))
      for c_milestone in l_milestonesPages:
        l_jsonMilestone = json.loads(c_milestone.json())

        if g_anon:
          l_jsonMilestone["author"]["name"] = anon("author", l_jsonMilestone["author"]["name"])

        l_bulkData.append({ "index":
                            {
                              "_index": "gitlab-a-day",
                              "_type" : "milestone",
                              "_id" : c_milestone.id
                            }
                          })
        l_bulkData.append({
          "milestone": l_jsonMilestone,
          "project": l_jsonProject,
          "group": l_ns,
          "@timestamp": l_jsonMilestone["created_at"]
        })
        if len(l_bulkData) > l_bulkSize:
          bulkInject(p_es, l_bulkData)
          l_written += len(l_bulkData) / 2
          l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(p_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "milestones injected")

######################################################################
# MAIN
g_args = parseArgs()

g_gl = gitlab.Gitlab.from_config() # ~/.python-gitlab.cfg
g_es = Elasticsearch(hosts=g_args.es, max_retries=2, timeout=200)
g_now = datetime.utcnow()
g_oldest = datetime.utcfromtimestamp(0)

g_userToTeam = readTeamFile(g_args.teams)

g_users = {}
users(g_gl, g_es, g_userToTeam)
g_groups = {}
g_projects = []
projects(g_gl, g_es, g_users)
groups(g_gl, g_es, g_groups)
issues(g_gl, g_es, g_projects)
milestones(g_gl, g_es, g_projects)
sys.exit(1)

######################################################################
# timeseries
def ts_project(p_date):
  l_projectDict = defaultdict(lambda: {"project_count": 0, "@timestamp": p_date})
  for c_project in l_projects:

    l_jsonProject = json.loads(c_project.json())
    l_createdAtFromStr = datetime.strptime(re.sub("\..*", "", l_jsonProject["created_at"]), "%Y-%m-%dT%H:%M:%S")
    l_createdAt = date(l_createdAtFromStr.year, l_createdAtFromStr.month, l_createdAtFromStr.day)

    if l_createdAt > p_date:
      continue
    l_strId = l_jsonProject["namespace"]["name"]
    l_ts = l_projectDict[l_strId]
    l_ts["project_count"] += 1
    l_ts["namespace"] = l_strId

  l_bulkData = []
  l_bulkSize = 500
  l_written = 0
  for c_key in l_projectDict.keys():
    l_bulkData.append({ "index":
                        {
                          "_index": "gitlab-in-time",
                          "_type" : "doc",
                          "_id" : c_key + "_" + str(p_date)
                        }
                      })
    l_bulkData.append(l_projectDict[c_key])
    if len(l_bulkData) > l_bulkSize:
      bulkInject(l_es, l_bulkData)
      l_written += len(l_bulkData) / 2
      l_bulkData = []
  if len(l_bulkData) > 0:
    bulkInject(l_es, l_bulkData)
    l_written += len(l_bulkData) / 2
    l_bulkData = []
  print("---", int(l_written), "namespace-ts injected")

l_today = date.today()
# back in time
for c_days in range(0, 400):
  l_delta = timedelta(c_days)
  ts_project(l_today - l_delta)
