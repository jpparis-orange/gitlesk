#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import gitlab

# local instance with docker image
# root:rootroot - CaZ9yd-bTecvicrRXPCZ
# jpparis:jpparisjpparis - iDJSuVe9dsR45mAaRYGz

l_gl = gitlab.Gitlab.from_config() # ~/.python-gitlab.cfg

######################################################################
# groups
try:
  l_group1 = l_gl.groups.create({"name": "group1", "path": "group1"})
  l_group2 = l_gl.groups.create({"name": "group2", "path": "group2"})
except gitlab.exceptions.GitlabCreateError as e:
  print("group not created:", e)
  l_group1 = l_gl.groups.search("group1")[0]
  l_group2 = l_gl.groups.search("group2")[0]

######################################################################
# users
l_userDict = { "email": "",
               "password": "",
               "username": "",
               "name": "" }
try:
  l_user1 = l_gl.users.create({"email": "x1@y.z",
                               "password": "________",
                               "username": "x1",
                               "name": "X1 Y"})
  l_user2 = l_gl.users.create({"email": "x2@y.z",
                               "password": "________",
                               "username": "x2",
                               "name": "X2 Y"})
except gitlab.exceptions.GitlabCreateError as e:
  print("user not created:", e)
  l_user1 = l_gl.users.list(username="x1")[0]
  l_user2 = l_gl.users.list(username="x2")[0]

######################################################################
# members
# GUEST_ACCESS = 10
# REPORTER_ACCESS = 20
# DEVELOPER_ACCESS = 30
# MASTER_ACCESS = 40
# OWNER_ACCESS = 50
try:
  l_group1.members.create({"user_id": l_user1.id,
                           "access_level": gitlab.MASTER_ACCESS})
  l_group1.members.create({"user_id": l_user2.id,
                           "access_level": gitlab.GUEST_ACCESS})
except gitlab.exceptions.GitlabCreateError as e:
  print("member not created:", e)

######################################################################
# projects
try:
  l_project1 = l_gl.projects.create({"name": "p1",
                                     "namespace_id": l_group1.id,
                                     "user_id": l_user1.id})
  l_project2 = l_gl.projects.create({"name": "p2",
                                     "namespace_id": l_group1.id,
                                     "user_id": l_user2.id})
except gitlab.exceptions.GitlabCreateError as e:
  print("project not created", e)
  l_project1 = l_gl.projects.get("group1/p1")
  l_project2 = l_gl.projects.get("group1/p2")


######################################################################
# issues
l_issue11 = l_project1.issues.create({"title": "I have a bug1",
                                      "description": "Something useful here."})
l_issue12 = l_project1.issues.create({"title": "I have a bug2",
                                      "description": "Something useful here."})
l_issue21 = l_project2.issues.create({"title": "I have another bug1",
                                      "description": "Something useful here."})
l_issue22 = l_project2.issues.create({"title": "I have another bug2",
                                      "description": "Something useful here."})

######################################################################
# issues
l_v1 = l_project1.milestones.create({'title': '1.0'})
l_v2 = l_project1.milestones.create({'title': '2.0'})
