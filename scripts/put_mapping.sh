#!/bin/bash

ES=${1:-localhost:9200}

TEMPLATE='gitlesk'
SHARDS=2
REPLICAS=1

HERE=$(cd $(dirname $0)/..; pwd)

exist=$(curl -XHEAD --write-out %{http_code} --silent --output /dev/null http://$ES/_template/$TEMPLATE)

[[ $exist == "200" ]] && \
  echo "Deleting ${TEMPLATE} template" && \
  curl -XDELETE "http://$ES/_template/${TEMPLATE}" && echo

echo "(Re)creating ${TEMPLATE} template"
cat ${HERE}/conf/mapping.json | sed -e '
s/SHARDS/'${SHARDS}'/g
s/REPLICAS/'${REPLICAS}'/g
' | curl -XPOST "http://$ES/_template/${TEMPLATE}" -d @- && echo

